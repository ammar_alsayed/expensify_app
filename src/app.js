
import React from "react";
import ReactDOM from "react-dom";
import {Provider} from 'react-redux';
import AppRouter, { history } from './routers/AppRouter.js';
import configureStore from './store/configureStore';
import {startDisplayExpenses} from "./actions/expenses";
import {login,logout} from './actions/auth';
import getVisibleExpenses from './selectors/expenses';
import LoadingPage from './components/LoadingPage'
import './styles/style.scss';
import 'normalize.css/normalize.css';
import {firebase} from './firebase/firebase';


const store = configureStore();

const jsx = (
    <Provider store = {store} >   
    <AppRouter/>
    </Provider>
)
// f unction to make sure the the app is rendered only once
let hasRendered = false; // we used let coz we will reassign it 
const renderApp =  () => {
    if(!hasRendered){
        ReactDOM.render(jsx, document.getElementById("app"));
        hasRendered = true; 
    }
    } 
ReactDOM.render(<LoadingPage />, document.getElementById("app"));


firebase.auth().onAuthStateChanged((user)=> {
    if(user){ // if user logged in
        store.dispatch(login(user.uid))
        console.log('logged in')
        // display his expenses
        store.dispatch(startDisplayExpenses()).then(() => { // on sccess
            renderApp();
            if(history.location.pathname === '/'){ // if user is in login page
                history.push('/dashboard') // redirect him to dashboard bage
            }
        });
    }else{
        store.dispatch(logout()) // dispatch logout
        console.log('logged out')
        renderApp(); 
        history.push('/');   
} 
}) 

