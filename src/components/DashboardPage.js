import React from "react";
import ExpenseList from './ExpenseList';
import FiltersExpenses from './FilterExpenses';
import ExpenseSummary from './ExpensesSummary';

const DashboardPage = () => (
    <div> 
        <ExpenseSummary />
        <FiltersExpenses />
        <ExpenseList />
    </div>
);
export default DashboardPage;