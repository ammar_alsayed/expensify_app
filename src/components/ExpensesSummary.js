import React from 'react';
import { connect } from 'react-redux';
import {Link} from 'react-router-dom';
import numeral from 'numeral';
import selectExpenses from '../selectors/expenses';
import selectExpensesTotal from '../selectors/expesnes_total';
// distruct props object in the argument
export const ExpensesSummary = ({expenseCount,expenseTotal}) => {
   
    const expenseWord = expenseCount === 1 ? 'expense' : 'expenses';
    expenseTotal = numeral(expenseTotal).format('$0,0.00');
    return(
        <div className='page-header'>
             <div className='content-container'>
                 <h2 className='page-header__title'> Viewing <span> {expenseCount} </span> {expenseWord} totalling: <span> {expenseTotal} </span></h2>
                <div className='page-header-action' >
                <Link to='/add' className='button' > Add Expense </Link>

                </div>
            </div>
        </div>
    )};

 const mapStateToProps = (state) => {
    const visibleExpenses = selectExpenses(state.expenses,state.filters);

    return{
        // only for the displayed expenses
        expenseCount: visibleExpenses.length,
        expenseTotal: selectExpensesTotal(visibleExpenses) 
    };
 }   
 export default connect(mapStateToProps)(ExpensesSummary);