// take all named exports and put them in variable firebase 
import firebase from 'firebase/app';
import 'firebase/auth';
import 'firebase/database';


// Initialize Firebase
const config = {
    apiKey: "AIzaSyB9HnPgTP11KmvrzjreNDXdPLh5I7HqaHs",
    authDomain: "expensify-8b3fd.firebaseapp.com",
    databaseURL: "https://expensify-8b3fd.firebaseio.com",
    projectId: "expensify-8b3fd",
    storageBucket: "expensify-8b3fd.appspot.com",
    messagingSenderId: "595624472444"
  };

  firebase.initializeApp(config);
  const database = firebase.database();
  // Web Authentication using google accounts
  const googleAuthProvider = new firebase.auth.GoogleAuthProvider(); 

export {firebase , googleAuthProvider, database as default };