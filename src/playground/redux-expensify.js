import { combineReducers, createStore} from 'redux';
import uuid from 'uuid';

// actions

 // - ADD_EXPENSE
 const addExpense = (
     { 
     description = '' , note = '',
     amount = 0 , 
     createAt = 0 
     } = {} ) => ({
     type: 'ADD_EXPENSE',
     expense: {
         // uuid is npm package using: yarn add uuid and importing it 
         id : uuid(), 
         description,
         note,
         amount,
         createAt
     }});
 // - REMOVE_EXPENSE
const removeExpense = ( (id) => ({
    type: 'REMOVE_EXPENSE',
    id
}));

 // - EDIT_EXPENSE
 const editExpense = ( (id, updates) => ({
    type:'EDIT_EXPENSE',
    id,
    updates
 }
 ));

 // - FILTER_TEXT
 const setTextFilter = ( (text = 'rent') =>({
    type: 'FILTER_TEXT',
    text 
 }));

 // - SORT_BY_AMOUNT
 const sortByAmount = ( () =>({
    type: 'SORT_BY_AMOUNT',

 }));
 
 // - FILTER_BY_DATE
 const sortByDate = ( () =>({
    type: 'SORT_BY_DATE',
    
 }));
 // - SET_START_DATE
 const setStartDate = ( (startDate) => ({
    type: 'SET_START_DATE',
    startDate
 }));
 // - SET_END_DATE
 const setEndDate = ( (endDate) => ({
    type: 'SET_END_DATE',
    endDate
 }));

 

// set up default expense state
const expensesReducerDefaultState = [];
// expense reducer
const expensesReducer = (state = expensesReducerDefaultState  , action) => {
    switch (action.type) {
        case 'ADD_EXPENSE':
        return [...state, action.expense];

        case 'REMOVE_EXPENSE':
        return state.filter( (expense) => ( //object destructuring
            expense.id !== action.id
        ));

        case 'EDIT_EXPENSE' :
        return state.map( (expense ) => {
            if( expense.id === action.id ){
              return{ 
                    ...expense, // pass exisiting state
                    ...action.updates // override only passed updates
                };
            }else{
                return expense; // no changes
            }
        });
        default:
            return state;
    }
};

const filterReducerDefaultState = {
    text: '',
    sortBy: 'date',
    startDate: undefined,
    endDate: undefined
};
const filterReducer = ( (state = filterReducerDefaultState , action) =>{
    switch (action.type) {
        case 'FILTER_TEXT':
        return {
            ...state,
            text: action.text
            
        };
        case 'SORT_BY_AMOUNT':
        return{
            ...state,
            sortBy: 'amount'
        };
        case 'SORT_BY_DATE':
        return{
            ...state,
            sortBy: 'date'
        };
        case 'SET_START_DATE':
        return{
            ...state,
            startDate: action.startDate
        };
        case 'SET_END_DATE':
        return{
            ...state,
            endDate: action.endDate
        }
        default: return state
    }
});
// get visive expenses
const getVisibleExpenses =  (expenses, {  sortBy, startDate,endDate,text } = filters) => {
    
    // get the expenses that matches filtering options only
   return expenses.filter( (expense) => {
       /*   cases for filtring: 
    
       DISPLAY ONLY THE EXPENSES CREATED BETWEEN  START DATE AND AND DATE if specified

       - if start date is not a number its means its undefined (default state),
       */
       const startDateMatch = typeof startDate !== 'number' // !== 'number' == undefined, he didn't filter by startdate
        || expense.createAt >= startDate; // if he filtered by start date, show only expenses created after startdate

       const  endDateMatch = typeof endDate !== 'number'
        || expense.createAt <= endDate; // if he filtered by end date, show only expenses created before end date
        
        // searc for the word in lowercased description
       const TextMatch = expense.description.toLowerCase().includes(text.toLowerCase());
         
       
        // if one is false the expense will bot be shown
       return startDateMatch && endDateMatch && TextMatch;  
   }).sort( (a , b) => {
       if(sortBy === 'date'){
           return a.createAt < b.createAt ? 1: -1;
       }else if(sortBy === 'amount'){
          return a.amount < b.amount ? 1: -1;
       }
   });

};

// store creation
const store = createStore(
    // combine multiple reducers, it takes abject of (key, value ) of
    // root state name and reduce that manage the state 
    combineReducers({
        expenses: expensesReducer,
        filters: filterReducer
    })
);

store.subscribe( () => {
    const state = store.getState(); // get all states
    const vsibleExpenses = getVisibleExpenses(state.expenses, state.filters);
    console.log(vsibleExpenses);
});

const expenseOne =  store.dispatch(addExpense({ description: 'Rent',amount: 200, createAt: 200  }));
const expenseTwo =store.dispatch(addExpense({ description: 'Coffee',amount: 200,createAt: -700 }));
const expenseThree = store.dispatch(addExpense({ description: 'Errands',note:'fruits and vegetables' 
,amount: 300 ,createAt: -344}));

 //store.dispatch(removeExpense(expenseOne.expense.id));

//store.dispatch(editExpense(expenseTwo.expense.id, {amount:40} ));

//store.dispatch(setTextFilter('rent'));
//store.dispatch(setTextFilter(''));

//store.dispatch(setStartDate(100));
//store.dispatch(setStartDate());

//store.dispatch(setEndDate(500));
//store.dispatch(setEndDate()); 

store.dispatch(sortByAmount());
//store.dispatch(sortByDate()); 



const demoState = {
    expenses:[
        {
        id: "1613102367699",
		description: "luctus lobortis. Class aptent",
		note: "tellus.",
		amount: 639700,
		createAt: 8
        }
    ],
    filters:{
        text: 'rent',
        sortBy: 'amount',  // date or amount
        startDate: undefined,
        endDate: undefined
    }
}