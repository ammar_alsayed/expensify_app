import React from 'react';
import { connect } from 'react-redux';
import {Route, Redirect} from 'react-router-dom';

 export const PublicRoute = ({
     isAuthenticated,
     component: Component,
     ...rest  // variable contain all props that are not desctructered
    }) => (
    <Route {...rest} component = { (props) => (
        isAuthenticated ? ( // if auth , redirect to dashboard
            <Redirect to='/dashboard' />
        ) :( // if not, reirect to login page
            <Component {...props} />
        )
    )} />
 );

 const mapPropsToState = (state) => ({
    isAuthenticated: !!state.auth.uid // get the boolean value
 }); 
 export default connect(mapPropsToState)(PublicRoute)